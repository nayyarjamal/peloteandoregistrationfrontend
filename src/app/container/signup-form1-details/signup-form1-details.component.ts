import { Component, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, Validators, ControlGroup, Control } from '@angular/common';
import { Router, ROUTER_DIRECTIVES} from '@angular/router';
import { GlobalService } from './../../GlobalService';
import { Button, InputText, Dropdown, SelectItem, TabView, TabPanel, AutoComplete} from 'primeng/primeng';
import { Calendar } from '../../components/datepicker/datepicker';
// import {CountryService} from 'primeng/primeng';

class playerDetail {
  "name": string;
  "mobile": string;
  "ssn": string;
  "email": string;
  "dob": string;
}

class registrationDetails {
  "school_name": string = "";
  "location": string = "";
  "zone": string = "";
}

class teamDetails {
  "school": number = null;
  "category": number = null;
  "team_name": string = "";
  "player_list": Array<any> = []
}

class playerDetails {
  "name": string = "";
  "mobile": string = "";
  "ssn": string = "";
  "email": string = "";
  "dob": string = "";
}

@Component({
  moduleId: module.id,
  selector: 'app-signup-form1-details',
  templateUrl: 'signup-form1-details.component.html',
  styleUrls: ['signup-form1-details.component.css'],
  directives: [ROUTER_DIRECTIVES, Button, InputText, Dropdown, TabView, TabPanel, AutoComplete, Calendar]
})

export class SignupForm1Details implements OnInit {

  registrationDetailsIns;
  registrationForm: ControlGroup;
  teamForm: ControlGroup;
  categoryList;
  teamDetailsIns;
  zoneList: SelectItem[];
  school_id: number = null;
  country: any;
  filteredLocation: any[];
  school_name: string = "";
  playerDetailsIns;
  category_id;
  @ViewChild('schoolName') schoolName: any;
  player_array: Array<any> = ['1'];
  playerIns;
  playerList;
  schoolExit:boolean = false;
  show:boolean=true;

  constructor(fb: FormBuilder, private base_path_service: GlobalService, private router: Router) {

    this.registrationDetailsIns = new registrationDetails();
    this.playerDetailsIns = new playerDetails();
    this.teamDetailsIns = new teamDetails();

    this.registrationForm = fb.group({
      school_name: [this.registrationDetailsIns.school_name, Validators.compose([Validators.required])],
      location: [this.registrationDetailsIns.location, Validators.compose([Validators.required])],
      zone: [this.registrationDetailsIns.zone, Validators.compose([Validators.required])]
    })

    this.teamForm = fb.group({
      team_name: [this.teamDetailsIns.team_name, Validators.compose([Validators.required])],
      mobile: [this.playerDetailsIns.mobile, Validators.compose([Validators.required, Validators.maxLength[10], Validators.minLength[10]])],
      ssn: [this.playerDetailsIns.ssn, Validators.compose([Validators.required])],
      email: [this.playerDetailsIns.zone, Validators.compose([Validators.required, Validators.pattern('[^@]+@[^@]+\.[a-zA-Z]{2,6}')])],
      name: [this.playerDetailsIns.name, Validators.compose([Validators.required])],
      dob: [this.playerDetailsIns.dob, Validators.compose([Validators.required])]
    })

  }

  ngOnInit() {
    // this.API_getZoneList();
    this.API_getSchool();
    this.API_getCategoryList();
    this.teamDetailsIns.player_list.push(new playerDetail());
  }

  API_getSchool() {
    var url = this.base_path_service.base_path_api() + "team/soccer_school/";
    this.base_path_service.GetRequest(url)
      .subscribe(
      res => {
        // console.log(res[0].json, "school_json");
        if (res[0].json.length > 0) {
          this.school_id = res[0].json[0].id;
          this.registrationDetailsIns.school_name = res[0].json[0].school_name;
          // console.log(res[0].json[0].location)
          this.schoolExit = true;
          this.API_getCategoryList();
          // this.registrationDetailsIns.location = res[0].json[0].location.location;
          // this.registrationDetailsIns.zone = res[0].json.zone[0].zone_name;
        }else {
          this.API_getCategoryList();
        }
      },
      err => {
        console.log("error")
      })
  }

  getPlayerList() {
    var url = this.base_path_service.base_path_api() + "user/player/?category=" + this.category_id;
    this.base_path_service.GetRequest(url)
      .subscribe(res => {
        // console.log(res[0].json);
        this.playerList = res[0].json;
      },
      err => {
        console.log("error")
      })
  }
  
  editDetails(){
    this.schoolExit = false;
  }

  removeExistedPlayer(player_id) {

    var url = this.base_path_service.base_path_api() + "user/player/" + player_id +"/";
    this.base_path_service.DeleteRequest(url)
      .subscribe(res => {
        // console.log(res[0].status);
        this.getPlayerList();
      },
      err => {
        console.log("error")
      })

    // for (var i = 0; i < this.playerList.players.length; i++) {
    //   if(this.playerList.players[i].id == player_id)
    //   var remove = this.playerList.players.indexOf(this.playerList.players[i]);
    //   this.playerList.players.splice(remove, 1);
    // }

    console.log(this.playerList.players);

  }

  filterLocation(event) {
    let query = event.query;
    console.log(query);
    var url = this.base_path_service.base_path_api() + "team/location/?city=" + query;
    this.base_path_service.GetRequest(url)
      .subscribe(
      res => {
        this.filteredLocation = this.filterCountry(query, res[0].json);
      },
      err => {
        console.log("error")
      });
  }

  filterCountry(query, location: any[]): any[] {
    let filtered: any[] = [];
    for (let i = 0; i < location.length; i++) {
      let city = location[i];
      if (city.location.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(city);
      }
    }
    return filtered;
  }

  API_getZoneList() {
    var url = this.base_path_service.base_path_api() + "team/zone/";
    this.base_path_service.GetRequest(url)
      .subscribe(
      res => {
        // console.log(res[0].json);
        // this.zoneList = res[0].json;
        this.zoneList = [];
        this.zoneList.push({ label: 'Select Zone', value: '' });
        for (let i = 0; i < res[0].json.length; i++) {
          this.zoneList.push({ label: res[0].json[i].zone_name, value: res[0].json[i].id });
        }
        
       
      },
      err => {
        console.log("error")
      })
  }

  API_getCategoryList() {
    var url = this.base_path_service.base_path_api() + "team/team_category/";
    this.base_path_service.GetRequest(url)
      .subscribe(
      res => {
        // console.log(res[0].json);
        this.categoryList = res[0].json;
        this.category_id = this.categoryList[0].id;
        // console.log(this.registrationDetailsIns.school_name+''+res[0].json[0].zone_name)
        this.teamDetailsIns.team_name = this.registrationDetailsIns.school_name+' '+res[0].json[0].category;
        this.getPlayerList();

      },
      err => {
        console.log("error")
      })
  }

  saveDetails(data) {
    this.schoolExit = true;
    data.zone = parseInt(data.zone);
    data.location = parseInt(data.location)
    console.log(data);
    var url = this.base_path_service.base_path_api() + "team/soccer_school/";
    this.base_path_service.PostRequest(url, this.registrationDetailsIns)
      .subscribe(
      res => {
        this.schoolExit = true;
        // console.log(res[0].status);
        // console.log(res[0].json)
        this.school_id = parseInt(res[0].json.id)
      },
      err => {
        console.log("error")
      })
  }

  addNew() {
    this.teamDetailsIns.player_list.push(new playerDetail());
    // console.log(this.teamDetailsIns)
  }
  datepicker(event, data) {
    data.dob = event;
  }

  removePlayer(i,data) {
    console.log(i, data);
    this.teamDetailsIns.player_list.splice(i, 1);
  }

  getCategory(event) {
    // console.log(event);
    var fullString = event.originalEvent.target.innerText
    // console.log(this.schoolName.nativeElement.value + ' ' + fullString);
    this.teamDetailsIns.team_name = this.schoolName.nativeElement.value + ' ' + fullString;
    // (<HTMLInputElement>document.getElementById('team_name')).value = this.schoolName.nativeElement.value+' '+fullString;
    for (let i = 0; i < this.categoryList.length; i++) {
      if (this.categoryList[i].category == fullString) {
        this.category_id = this.categoryList[i].id;
        // console.log(this.categoryList[i].id);
        this.getPlayerList();
      }
    }
  }

  createTeam() {
    // console.log(this.teamDetailsIns.player_list)
    
    // console.log(this.playerIns, this.teamDetailsIns.player_list, this.player_array.length)

    this.teamDetailsIns.school = this.school_id;
    this.teamDetailsIns.category = this.category_id;
    // console.log(this.teamDetailsIns)
    var url = this.base_path_service.base_path_api() + "user/player/";
    this.base_path_service.PostRequest1(url, this.teamDetailsIns)
      .subscribe(
      res => {
        console.log(res[0].status)
      },
      err => {
        console.log("error")
      })
  }
  getLocation(event) {
    // console.log(event);
    this.registrationDetailsIns.location = event.id;
  }

  logout() {
    // console.log("logout")
    localStorage.clear();
    this.router.navigate(['/home', 'login']);
  }
}
