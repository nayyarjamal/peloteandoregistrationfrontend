import { Component, OnInit } from '@angular/core';
import { LoginService} from './../../services/login/login.service';
import {Router} from '@angular/router'
import { GlobalService } from './../../GlobalService';
import {Button, InputText} from 'primeng/primeng';

class userDetails {
  "new_password":"";
  "confirm_password" :"";
}

@Component({
  moduleId: module.id,
  selector: 'app-reset-password',
  templateUrl: 'reset-password.component.html',
  styleUrls: ['reset-password.component.css'],

  directives:[Button, InputText],
  providers: [LoginService]
})
export class ResetPasswordComponent implements OnInit {
  
  userDetailsIns;
  
  private paramsSub: any;
  private user: string;
  private key: string;
  public aToken: string;
  public uid: number;
  public newPass: string;
  userKey;
  
  constructor(private login_service: LoginService, private router: Router, private base_path_service:GlobalService) {
   
   this.userDetailsIns = new userDetails();
   this.userKey = {
     "user":"",
     "key":""
   }
  }

  ngOnInit() {
    this.paramsSub= this.router
    .routerState
    .queryParams
    .subscribe(params => {
      this.user = params['user'];
      this.key=params['key'];
      console.log(`value:${this.user}id:${this.key}`);
      this.verifyKey();
    });
  }

  verifyKey() {
    this.userKey.user = this.user;
    this.userKey.key = this.key;

    let url: string = this.login_service.baseUrl + '/peloteando/reset_password/';
    console.log("method called");
    this.login_service.forgetPasswordRequest(this.userKey, url)
      .subscribe(
        res => {
          localStorage.setItem("forget", JSON.stringify(res));
          this.aToken = res.token.acces_token;
          this.uid = res.info.account_id;
        },
        err=>{
          console.log("error")
      })

  }
  setPassword(data) {
    let a: any = {
      "user_id": this.uid,
      "new_password": data.new_password
    };
    let url = this.base_path_service.base_path_api()+"peloteando/set_password/";

    this.login_service.setPasswordRequest(a, url, this.aToken)
      .subscribe(
      res => {
        console.log(res.status);
        if (localStorage.getItem("forget") != null)
          localStorage.removeItem("forget");
        if (res.status == 205) {
          this.router.navigate(['/home', 'login'])
        }
      },
      err => {
        console.log("error")
      })
  }

}
